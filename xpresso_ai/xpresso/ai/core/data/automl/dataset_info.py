""" Class definition of automl Info """

from xpresso.ai.core.logging.xpr_log import XprLogger

__all__ = ['DatasetInfo']
__author__ = 'Srijan Sharma'

# This is indented as logger can not be serialized and can not be part
# of automl
logger = XprLogger()


class DatasetInfo:
    """ DatasetInfo contains the detailed information about the
    dataset. This information contains the attribute list,
    attribute type, attribute metrics"""

    def __init__(self):
        self.attributeInfo = list()
        self.metrics = dict()
        return
